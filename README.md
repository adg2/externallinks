# Additional material

### 05.09.2019

[Activity lifecycle](https://developer.android.com/guide/components/activities/activity-lifecycle)

[Android onSaveInstanceState](https://www.journaldev.com/22621/android-onsaveinstancestate-onrestoreinstancestate)

### 07.09.2019

[Menus](https://developer.android.com/guide/topics/ui/menus)

[Add menu items in action bar](https://www.youtube.com/watch?v=kknBxoCOYXI)

### 10.09.2019

[Debugger](https://developer.android.com/studio/debug)

### 12.09.2019

[App resources overview](https://developer.android.com/guide/topics/resources/providing-resources)

[Support different pixel densities](https://developer.android.com/training/multiscreen/screendensities)

[Generic icon generator](https://romannurik.github.io/AndroidAssetStudio/icons-generic.html#source.type=clipart&source.clipart=ac_unit&source.space.trim=1&source.space.pad=0&size=32&padding=8&color=rgba(0%2C%200%2C%200%2C%200.54)&name=ic_ac_unit)

### 14.09.2019

[RecyclerView](https://developer.android.com/guide/topics/ui/layout/recyclerview)

[Working with RecyclerView](https://www.androidhive.info/2016/01/android-working-with-recycler-view/)
